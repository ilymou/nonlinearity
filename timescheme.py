#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: i.moufid
"""

import sys
import numpy as np

"""
This module file provides time scheme
"""

class TimeScheme(object):
    """
    Class that manages the time scheme
    Purpose: Compute the solution known at a time t_n
    for the next time step
    """

    def __init__(self, t0, u0):
        """
        Constructor : Define variables associated to
        the time scheme
        """
        # Attributes
        self.t0 = t0
        self.u0 = u0

        self.un   = [u0]
        self.tn   = [t0]
        return

    def get_sol(self):
        """
        Return the solution containing in the TimeScheme class
        Output:
            un: array containing the solution at each time step
        """
        return self.un

# --- --- ---

def EE(u,F,t,dt):
    """
    Solve du/dt = F(t,u) with an Explicit Euler scheme

    Parameters
    ----------
    scheme : object of TimeScheme class
        contains functions of the studied problem
    t : float
        Actual time where the solution is known

    Returns
    -------
    un : float
        solution 'u' at the time t+dt
    """
    return u + dt * F(t, u)

def RK4(u,F,t,dt):
    """
    Solve du/dt = F(t,u) with a 4 order Runge-Kutta scheme

    Parameters
    ----------
    u : object of TimeScheme class
        contains functions of the studied problem
    F : function
        right hand side of the equation
    t : float
        actual time where the solution is known

    Returns
    -------
    un : float
        solution 'u' at the time t+dt
    """
    k1 = F(t          , u)
    k2 = F(t + dt / 2., u + (dt / 2.) * k1)
    k3 = F(t + dt / 2., u + (t / 2.) * k2)
    k4 = F(t + dt     , u + dt * k3)

    return u + (dt / 6) * (k1 + 2.*k2 + 2.*k3 + k4)