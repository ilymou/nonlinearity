#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: i.moufid
"""

import numpy as np
import time
import matplotlib.pyplot as plt
import timescheme as ts

# %% Paramater Initialisation
u0   = 1.
t0   = 0.
tf   = 5.
dt   = 0.1

nIt = int((tf - t0) / dt)
last = False
if nIt * dt < tf: last = True

a_nl = 0.5
def F(t,u):
    return - a_nl * np.abs(u) * u

# %% Plot information
print("Equation : du/dt = {:.2} |u|u".format(-a_nl))
print("  -Initial time : {:}".format(t0))
print("  -Final time   : {:}".format(tf))
print("  -Time step    : {:}".format(dt))

# %% Time Scheme
problem = ts.TimeScheme(t0, u0)

t = t0
tic = time.perf_counter()
for i in range(nIt):
    problem.un.append(ts.RK4(problem.un[-1],F,t,dt))
    t = t + dt
    problem.tn.append(t)

if last:
    dt = tf - nIt * dt
    problem.un.append(ts.RK4(problem.un[-1],F,t,dt))
    t = t + dt
    problem.tn.append(t)
toc = time.perf_counter()

# %% Plot information
print("\nComputation time       : {:.2} s".format(toc-tic))
print("Solution at final time : {}".format(problem.un[-1]))
plt.figure()
#plt.yscale('log')
plt.plot(problem.tn, problem.un)
plt.show()